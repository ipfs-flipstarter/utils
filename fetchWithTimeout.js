module.exports = async function fetchWithTimeout(url, timeout = 5000) {
  const controller = new AbortController();
  let id;
  if (timeout) id = setTimeout(() => controller.abort(), timeout);
  const result = await fetch(url, { method: "POST", signal: controller.signal, mode: 'cors' });
  if (timeout) clearTimeout(id);
  return result;
}
