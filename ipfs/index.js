const { createLink, createNode, encode } = require('@ipld/dag-pb')
const { CID } = require('multiformats/cid')

module.exports = {
  /**
 * 
 * @param {import('ipfs').IPFS} ipfs 
 * @param {*} campaignData 
 * @returns {Promise<CID>}
 */
  create: async (ipfs, baseDag, campaignData) => {  
    const data = { ...campaignData, version: "0.0.1" };
    const { cid, size } = await ipfs.add(JSON.stringify(data));
    const campaignLink = createLink("campaign.json", size, cid);

    const baseLinks = baseDag.Links.filter((link) => link.Name !== 'campaign.json');
    const clientDagNode = createNode(baseDag.Data, [ ...baseLinks, campaignLink ]);
    
    return await ipfs.dag.put(clientDagNode, { storeCodec: 'dag-pb' });
  },
  validate: async (ipfs, baseDag, campaignData, expectedCid) => {
    try {
      const { cid:expectedCampaignJsonCid, size:expectedCampaignJsonSize } = await ipfs.add(JSON.stringify(campaignData), { onlyHash: true });
      const expectedCampaignJsonLink = createLink("campaign.json", expectedCampaignJsonSize, expectedCampaignJsonCid);
    
      //Add link to baseClientDag
      const expectedCampaignDagNode = createNode(baseDag.Data, [ ...baseDag.Links, expectedCampaignJsonLink ]);
    
      //Hash the result
      const hasher = await ipfs.hashers.getHasher("sha2-256");
      const expectedCampaignRawDag = encode(expectedCampaignDagNode);
      const expectedCampaignDagHash = hasher.digest(expectedCampaignRawDag);
      const expectedCampaignDagCid = CID.createV0(expectedCampaignDagHash)
      
      return { success: expectedCampaignDagCid.toString() !== expectedCid.toString() }
    
    } catch (error) {

      return { success: false, error };
    }
  }
}